# Calcal - Manage Absence through Slack

Calcal is an easy way to manage absence.

You can set it up on your slack with any slash command.
In the documentation I assume this is `/timeoff`, which is what we have
set up at GitLab.

To get started, simply type `/timeoff hello`.

Right now, Calcal is designed to only return full-day events.
This can be changed in the future, but for absence is more convenient.

## Installation

Run the rails app somewhere, like Heroku or one of your servers.

You need to set an environmental variable `SLACK_TOKEN`, in order
for the bot to work. You can find this token in the Slack slash command config.

## Contributing

This is a simple Rails-API app.

Feel free to work on any of the To do's below and submit a MR.

## TODO

- request vacation today `/timeoff today?`
- request vacation this week `/timeoff week?`
- CRUD vacation
- set up iCal integration

## Syntax

```
/timeoff new DATE to DATE because REASON
```

`DATE` can be any formatting. If it doesn't work, it'll complain.
You can use things like `tomorrow`, `next week` or `feb 12`.

```
/timeoff index
```

will return your dates.

```
/timeoff help
```

will return the syntax

```
/timeoff hello
```

will return 'Hello!'

```
/timeoff USERNAME
```

will return all dates if that person has at least one

```
/timeoff calendar
```

Will give you the ical link

## iCal Links

```yourdomain.com/vacations/:slack_user_id/cal.ics```

for the calendar for one person

```yourdomain.com/vacations.ics```

for a single calendar with all vacations
