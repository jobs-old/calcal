class ApplicationController < ActionController::API
  before_filter :check_slack_token

  private

  def check_slack_token
    params[:token] == ENV['SLACK_TOKEN']
  end
end
