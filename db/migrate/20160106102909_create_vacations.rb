class CreateVacations < ActiveRecord::Migration
  def change
    create_table :vacations do |t|
      t.datetime :start
      t.datetime :end
      t.string :reason
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
