class IncomingSlackCommandsController < ApplicationController
  def incoming
    unless params[:text].blank?
      @command = params[:text].split(' ')
      check_command
    else
      @outgoing = "How are you?\n
      Get started by adding a date with:\n
      /timeoff DATE to DATE because REASON\n\n

      Or help building this bot by going to https://gitlab.com/JobV/calcal"
    end
    render json: slack_message
  end

  private

  def check_command
    case @command[0]
    when 'index' || 'me' || 'mine' || 'I'
      index
    when 'new'
      parse_new
    when 'debug'
      debug
    when 'calendar'
      calendar
    when 'hello'
      @outgoing = 'Hello!'
    when 'remove'
      remove
    when 'repo' || 'repository'
      @outgoing = "https://gitlab.com/JobV/calcal"
    when 'help' || 'syntax' || 'how'
      @outgoing = "/timeoff new DATE to DATE because REASON"
    else
      is_this_someone?
    end
  end

  def calendar
    @outgoing = "iCal feed for everyone: #{request.base_url}/ical.ics"
  end

  # /timeoff new feb 12 to feb 15 vacation
  def parse_new
    hash = Hash[@command.map.with_index.to_a]
    pos_of_to = hash['to']
    pos_of_because = hash['because']

    if pos_of_to && pos_of_because

      # find everything before and after that, as start and end
      start_of_vacation = @command[1..pos_of_to-1].join(" ")
      end_of_vacation = @command[pos_of_to+1..pos_of_because].join(" ")
      @reason = @command[pos_of_because+1..-1].join(" ")

      @start_of_vacation = Chronic.parse(start_of_vacation)
      @end_of_vacation = Chronic.parse(end_of_vacation)

      create_vacation
    else
      @outgoing = "Wrong syntax. Make sure to use /timeoff new DATE to DATE because REASON"
    end
  end

  def create_vacation
    Vacation.create(
      start: @start_of_vacation,
      end: @end_of_vacation,
      reason: @reason,
      user_id: params[:user_id],
      user_name: params[:user_name]
    )
    @outgoing = "Vacation created! Thanks #{params[:user_name]}!"
  end

  def remove
    unless @command[1].blank?
      v = Vacation.find(@command[1].to_i)
      if v.user_name == params[:user_name]
        v.destroy ? @outgoing = "Destroyed!" : @outgoing = "Something went wrong. Are you sure the ID is correct?"
      else
        @outgoing = "This is not yours!"
      end
    else
      @outgoing = "Add the ID of one of your vacations."
    end
  end

  def is_this_someone?
    @vacations = Vacation.where(user_name: @command[0].downcase)
    if @vacations.empty?
      @outgoing = "I don't know #{@command[0].to_s} yet.."
    else
      render_vacations_for(@command[0].downcase)
    end
  end

  def index
    render_vacations_for(params[:user_name])
    @outgoing << "iCal feed for your calendar: #{request.base_url}/ical/#{params[:user_id]}/cal.ics"
  end

  def render_vacations_for(user_name)
    @vacations = Vacation.where(user_name: user_name)
    @outgoing = "*#{user_name} is off between the following dates:*\n"
    @vacations.each do |vacation|
      @outgoing << "- ##{vacation.id} #{pretty_date(vacation.start)} to #{pretty_date(vacation.end)}: #{vacation.reason}\n"
    end
  end

  def debug
    @vacations = Vacation.where(user_name: params[:user_name])
    @outgoing = ""
    @vacations.each do |vacation|
      @outgoing << "id:#{vacation.id}\nuser_id:#{vacation.user_id}\nuser_name:#{vacation.user_name}\nstart: #{pretty_date(vacation.start)}\nend:#{pretty_date(vacation.end)}\nreason: #{vacation.reason}\n\n"
    end
  end

  def slack_message
    {
      "response_type" => "in_channel",
      "text" => "#{@outgoing}"
    }
  end

  def pretty_date(date)
    date.strftime("%b %e %Y")
  end
end

# This is what slack sends us

# token=3WcBm2GI6U9MDFRC25QNn9je
# team_id=T0001
# team_domain=example
# channel_id=C2147483705
# channel_name=test
# user_id=U2147483697
# user_name=Steve
# command=/weather
# text=94070
# response_url=https://hooks.slack.com/commands/1234/5678
