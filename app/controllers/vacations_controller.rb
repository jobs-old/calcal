class VacationsController < ApplicationController
  include ActionController::MimeResponds
  before_action :set_vacation, only: [:show, :update, :destroy]

  def ical
    vacations = Vacation.all
    calendar = Icalendar::Calendar.new
    vacations.each do |vacation|
      calendar.add_event(vacation.to_ics)
    end

    render text: calendar.to_ical
  end

  def ical_person
    vacations = Vacation.where(user_id: params[:user_id])
    calendar = Icalendar::Calendar.new
    vacations.each do |vacation|
      calendar.add_event(vacation.to_ics)
    end

    respond_to do |format|
      format.ics { render text: calendar.to_ical }
    end
  end

  # GET /vacations
  # GET /vacations.json
  def index
    @vacations = Vacation.all

    render json: @vacations
  end

  # GET /vacations/1
  # GET /vacations/1.json
  def show
    render json: @vacation
  end

  # POST /vacations
  # POST /vacations.json
  def create
    @vacation = Vacation.new(vacation_params)

    @vacation.start = Chronic.parse(@vacation.start)
    @vacation.end = Chronic.parse(@vacation.end)

    if @vacation.save
      render json: @vacation, status: :created, location: @vacation
    else
      render json: @vacation.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /vacations/1
  # PATCH/PUT /vacations/1.json
  def update
    @vacation = Vacation.find(params[:id])

    if @vacation.update(vacation_params)
      head :no_content
    else
      render json: @vacation.errors, status: :unprocessable_entity
    end
  end

  # DELETE /vacations/1
  # DELETE /vacations/1.json
  def destroy
    @vacation.destroy

    head :no_content
  end

  private

    def set_vacation
      @vacation = Vacation.find(params[:id])
    end

    def vacation_params
      params.require(:vacation).permit(:start, :end, :reason, :user_id, :name)
    end
end
