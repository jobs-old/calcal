class MakeUserIdString < ActiveRecord::Migration
  def change
    change_column :vacations, :user_id, :string
  end
end
