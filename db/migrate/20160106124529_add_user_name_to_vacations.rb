class AddUserNameToVacations < ActiveRecord::Migration
  def change
    add_column :vacations, :user_name, :string
  end
end
